FROM node:16.13.0-alpine

COPY . .

RUN npm i

RUN npx prisma generate

EXPOSE 6000