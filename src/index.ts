import express from 'express'

import { PrismaClient } from '@prisma/client'

const prisma = new PrismaClient();
const app = express();

app.listen(6000, () => {
  console.log("listening to port:6000, hahaha");
})

// app.get('/user', async (req, res) => {
//   const posts = await prisma.user.findMany();
//   res.json(posts)
// })

app.get('/user', async (req, res) => {
  const posts = await prisma.user.findMany();
  res.json(posts)
})

app.post('/addUser', async (req, res) => {
  const user = await prisma.user.create({
    data: {
      email: req.query.email as string,
      name: req.query.name as string,
    },
  })
  console.log(user);
  res.status(200);
})